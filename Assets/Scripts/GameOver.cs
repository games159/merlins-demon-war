using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public TMP_Text scoreText = null;
    public TMP_Text killsText = null;

    private void Awake()
    {
        scoreText.text = "Score: " + GameController.instance.playerScore.ToString();
        killsText.text = "Demons killed: " + GameController.instance.playerKills.ToString();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}
